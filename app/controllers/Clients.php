<?php

/**
 * Every page loads from view folder
 * in order to load a view inside a folder of the view folder
 * the folder/filename must be parsed
 */
class Clients extends Controller
{
    public function __construct()
    {
        $this->clientModel = $this->model('Client');
    }

    public function index()
    {
        $items = $this->clientModel->list();
        $groups = $this->clientModel->groups('id, nombre');
        $data = [
            'title' => "Listado de clientes",
            'items' => $items,
            'groups' => $groups

        ];
        $this->view('clients/index', $data);
    }

    public function list(){
        $items = $this->clientModel->list();
    }

    public function groups()
    {        
        $items = $this->clientModel->groups();
        $data = [
            'title' => "Listado de grupos de clientes",
            'items' => $items
        ];
        $this->view('clients/groups', $data);
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'nombre' => trim($_POST['nombre']),
                'apellido' => trim($_POST['apellido']),
                'email' => trim($_POST['email']),
                'id_grupo_cliente' => trim($_POST['id_grupo_cliente']),
                'observaciones' => trim($_POST['observaciones'])
            ];

            //validate
            if (empty($data['nombre']) or empty($data['apellido']) or empty($data['email']) or empty($data['id_grupo_cliente'])) {
                $data['msg_err'] = 'Error al recivbir los datos';
            }

            if (empty($data['msg_err'])) {
                // die('success');
                if ($item = $this->clientModel->add($data)) {

                } else {
                    die('algo salio mal');
                }
            }
        } 
    }

    public function addGroup()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'nombre' => trim($_POST['nombre']),
                'descripcion' => trim($_POST['descripcion'])
            ];

            //validate
            if (empty($data['nombre'])) {
                $data['msg_err'] = 'Error al recivbir los datos';
            }

            if (empty($data['msg_err'])) {
                // die('success');
                if ($item = $this->clientModel->addGroup($data)) {

                } else {
                    die('algo salio mal');
                }
            }
        } 
    }


    public function edit($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'id' => $id,
                'nombre' => trim($_POST['nombre']),
                'apellido' => trim($_POST['apellido']),
                'email' => trim($_POST['email']),
                'id_grupo_cliente' => trim($_POST['id_grupo_cliente']),
                'observaciones' => trim($_POST['observaciones'])
            ];

            //validate
            if (empty($data['nombre']) or empty($data['apellido']) or empty($data['email']) or empty($data['id_grupo_cliente'])) {
                $data['msg_err'] = 'Error al recivbir los datos';
            }

            if (empty($data['msg_err'])) {
                // die('success');
                if ($this->clientModel->edit($data)) {

                } else {
                    die('algo salio mal');
                }
            }
        }   
    }

    public function editGroup($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'id' => $id,
                'nombre' => trim($_POST['nombre']),
                'descripcion' => trim($_POST['descripcion'])
            ];

            //validate
            if (empty($data['nombre'])) {
                $data['msg_err'] = 'Error al recivbir los datos';
            }

            if (empty($data['msg_err'])) {
                // die('success');
                if ($this->clientModel->editGroup($data)) {

                } else {
                    die('algo salio mal');
                }
            }
        }   
    }

    public function delete($id)
    {
        $item = $this->clientModel->delete($id);
    }

    public function deleteGroup($id)
    {
        $item = $this->clientModel->deleteGroup($id);
    }
}

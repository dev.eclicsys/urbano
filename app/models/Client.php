<?php

// namespace App\Models;

class Client
{
    private $db;

    /**
     * Client constructor.
     * @param null $data
     */
    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * 
     * Clients
     * 
     */

    public function list()
    {
        $this->db->query('SELECT *,                      
                      c.id as id,
                      c.nombre as nombre,
                      g.id as id_grupo_cliente,
                      g.nombre as grupo
                      FROM clientes AS c
                      INNER JOIN clientes_grupos AS g
                      ON c.id_grupo_cliente = g.id
                      ORDER BY c.id DESC
                      ');
        $results = $this->db->resultSet();
        return $results;
    }

    public function show($id)
    {

    }

    public function add($data)
    {
        settype($data['id_grupo_cliente'], "integer");

        $this->db->query('INSERT INTO clientes (nombre, apellido, email, id_grupo_cliente, observaciones) 
                          VALUES(:nombre, :apellido, :email, :id_grupo_cliente, :observaciones)');
        $this->db->bind(':nombre', $data['nombre']);
        $this->db->bind(':apellido', $data['apellido']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':id_grupo_cliente',$data['id_grupo_cliente']);
        $this->db->bind(':observaciones', $data['observaciones']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }

    }

    public function edit($data)
    {
        settype($data['id'], "integer");
        settype($data['id_grupo_cliente'], "integer");
        var_dump($data);
        $this->db->query('UPDATE clientes 
                          SET nombre=:nombre, 
                              apellido=:apellido,
                              email=:email,
                              id_grupo_cliente=:id_grupo_cliente,
                              observaciones=:observaciones
                          WHERE id =:id');
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':nombre', $data['nombre']);
        $this->db->bind(':apellido', $data['apellido']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':id_grupo_cliente',$data['id_grupo_cliente']);
        $this->db->bind(':observaciones', $data['observaciones']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $this->db->query('DELETE FROM clientes where id=:id');
        $this->db->bind(':id', $id);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * Clients Groups
     * 
     */

    public function groups($select="*")
    {
        $select = !null ? $select : "*";
        $this->db->query('SELECT '.$select.'
                      FROM clientes_grupos AS g
                      ORDER BY g.id ASC
                      ');
        $results = $this->db->resultSet();
        return $results;
    }

    public function group($id)
    {

    }

    public function addGroup($data)
    {
        $this->db->query('INSERT INTO clientes_grupos (nombre, descripcion) 
                          VALUES(:nombre, :descripcion)');
        $this->db->bind(':nombre', $data['nombre']);
        $this->db->bind(':descripcion', $data['descripcion']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function editGroup($data)
    {
        $this->db->query('UPDATE clientes_grupos
        SET nombre=:nombre, 
            descripcion=:descripcion
        WHERE id =:id');
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':nombre', $data['nombre']);
        $this->db->bind(':descripcion', $data['descripcion']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteGroup($id)
    {
        $this->db->query('DELETE FROM clientes_grupos where id=:id');
        $this->db->bind(':id', $id);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    
}

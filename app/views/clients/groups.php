<?php require APPROOT . '/views/inc/header.php'; ?>
<?php require APPROOT . '/views/inc/navbar.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?php echo $data['title'] ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?php echo $data['title'] ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><button class="btn btn-primary" onclick=" open_add_form()">Nuevo Grupo</button></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Grupo</th>
                    <th>Descripción</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($data['items'] as $item) : ?>
                    <tr data-id="<?= $item->id; ?>"> 
                      <td><?= $item->id; ?></td>
                      <td><?= $item->nombre ?></td>
                      <td><?= $item->descripcion ?></td>
                      <td>
                        <?php $encode = str_replace("\"", "'", json_encode($item)); ?>
                        <a href="#" onclick="open_add_form(<?= $encode ?>)">
                          <i class="fas fa-edit text-success"></i>
                        </a>
                      </td>
                      <td>
                        <button type="button" class="btn btn-link delete" value="<?= $item->id; ?>">
                          <i class="fas fa-trash text-danger"></i>
                        </button>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Grupo</th>
                    <th>Descripción</th>
                    <th></th>
                    <th></th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="modal_group_form" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form id="group_form" name="group_form" action="">
          <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre">
            <label class="error" for="nombre" id="nombre_error">Este campo es requerido.</label>
          </div>
          <div class="form-group">
            <label>Descripción</label>
            <textarea name="descripcion" id="descripcion" class="form-control" rows="3" placeholder="Enter ..."></textarea>
          </div>
          <input type="hidden" name="id" class="form-control" id="id">
        </form>
      </div>
      <div class="modal-footer">
        <button id="submit_group_form" type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->

<?php require APPROOT . '/views/inc/footer.php'; ?>

<!-- DataTables  & Plugins -->
<script src="<?= URLROOT; ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= URLROOT; ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= URLROOT; ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= URLROOT; ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?= URLROOT; ?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= URLROOT; ?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?= URLROOT; ?>plugins/jszip/jszip.min.js"></script>
<script src="<?= URLROOT; ?>plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?= URLROOT; ?>plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?= URLROOT; ?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?= URLROOT; ?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?= URLROOT; ?>plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- Page specific script -->
<script>
  $(function() {
    $("#example1").DataTable({
      "responsive": true,
      "lengthChange": false,
      "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });

    $("#submit_group_form").click(function() {

      var id = $("input#id").val();
      var nombre = $("input#nombre").val();
      var descripcion = $("input#descripcion").val();

      // validate and process form here
      $('.error').hide();

      if (nombre == "") {
        $("label#nombre_error").show();
        $("input#nombre").focus();
        return false;
      }

      if (id > 0)
        var uri = "editGroup/" + id;
      else
        var uri = "addGroup";

      $.ajax({
        type: "POST",
        url: uri,
        data: $("#group_form").serialize(),
        async: false,
        cache: false,
        success: function(data) {
          $("#modal_group_form").modal('hide');
          if (id > 0) {
            location.reload(); 
          } else {
            location.reload();
          }
        }
      });
      return false;
    });

    $(".delete").click(function() {
      $.ajax({
        type: "POST",
        url: "deleteGroup/" + $(this).val(),
        data: $(this).val(),
        async: false,
        cache: false,
        success: function(data) {
          location.reload();
        }
      });
      return false;
    });
  });

  function open_add_form(item = null) {

    if (item === null) {

      $("#id").val("");
      $("#nombre").val("");
      $("#descripcion").val("");

    } else {

      $("#id").val(item['id']);
      $("#nombre").val(item['nombre']);
      $("#descripcion").val(item['descripcion']);
    }

    $("#modal_group_form").modal()
  }
</script>



</script>
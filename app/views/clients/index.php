<?php require APPROOT . '/views/inc/header.php'; ?>
<?php require APPROOT . '/views/inc/navbar.php'; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?php echo $data['title'] ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?php echo $data['title'] ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><button class="btn btn-primary" onclick=" open_add_form()">Nuevo Cliente</button></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Email</th>
                    <th>Grupo</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($data['items'] as $item) : ?>
                    <tr data-id="<?= $item->id; ?>">
                      <td><?= $item->id; ?></td>
                      <td><?= $item->nombre ?></td>
                      <td><?= $item->apellido ?></td>
                      <td><?= $item->email ?></td>
                      <td><?= $item->grupo ?></td>
                      <td>
                        <?php $encode = str_replace("\"", "'", json_encode($item)); ?>
                        <a href="#" onclick="open_add_form(<?= $encode ?>)">
                          <i class="fas fa-edit text-success"></i>
                        </a>
                      </td>
                      <td>
                        <button type="button" class="btn btn-link delete" value="<?= $item->id; ?>">
                          <i class="fas fa-trash text-danger"></i>
                        </button>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Email</th>
                    <th>Grupo</th>
                    <th>Editar</th>
                    <th>Borrar</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="modal_client_form" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form id="client_form" name="client_form" action="">
          <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre">
            <label class="error" for="nombre" id="nombre_error">Este campo es requerido.</label>
          </div>
          <div class="form-group">
            <label for="apellido">Apellido</label>
            <input type="text" name="apellido" class="form-control" id="apellido" placeholder="Apellido">
            <label class="error" for="apellido" id="apellido_error">Este campo es requerido.</label>
          </div>
          <div class="form-group">
            <label for="inputEmail">Email address</label>
            <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
            <label class="error" for="email" id="email_error">Este campo es requerido.</label>
          </div>
          <div class="form-group">
            <label>Grupo</label>
            <select id="idGrupoCliente" name="id_grupo_cliente" class="custom-select">
              <?php foreach ($data['groups'] as $group) : ?>
                <option value="<?= $group->id; ?>"><?= $group->nombre; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="form-group">
            <label>Observaciones</label>
            <textarea name="observaciones" id="observaciones" class="form-control" rows="3" placeholder="Enter ..."></textarea>
          </div>
          <input type="hidden" name="id" class="form-control" id="id">
        </form>
      </div>
      <div class="modal-footer">
        <button id="submit_client_form" type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->

<?php require APPROOT . '/views/inc/footer.php'; ?>

<!-- DataTables  & Plugins -->
<script src="<?php echo URLROOT; ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo URLROOT; ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo URLROOT; ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo URLROOT; ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo URLROOT; ?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo URLROOT; ?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo URLROOT; ?>plugins/jszip/jszip.min.js"></script>
<script src="<?php echo URLROOT; ?>plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo URLROOT; ?>plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo URLROOT; ?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo URLROOT; ?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo URLROOT; ?>plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- Page specific script -->
<script>
  $(function() {
    $("#example1").DataTable({
      "responsive": true,
      "lengthChange": false,
      "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });

    $("#submit_client_form").click(function() {

      var id = $("input#id").val();
      var nombre = $("input#nombre").val();
      var apellido = $("input#apellido").val();
      var email = $("input#email").val();
      var idGrupoCliente = $("input#idGrupoCliente").val();
      var observaciones = $("input#observaciones").val();

      // validate and process form here
      $('.error').hide();

      if (nombre == "") {
        $("label#nombre_error").show();
        $("input#nombre").focus();
        return false;
      }
      if (apellido == "") {
        $("label#apellido_error").show();
        $("input#apellido").focus();
        return false;
      }
      var email = $("input#email").val();
      if (email == "") {
        $("label#email_error").show();
        $("input#email").focus();
        return false;
      }

      if (id > 0)
        var uri = "clients/edit/" + id;
      else
        var uri = "clients/add";

      $.ajax({
        type: "POST",
        url: uri,
        data: $("#client_form").serialize(),
        async: false,
        cache: false,
        success: function(data) {
          $("#modal_client_form").modal('hide');
          if (id > 0){
            location.reload();
          } else {
            location.reload();
          }    
        }
      });
      return false;
    });

    $(".delete").click(function() {
      $.ajax({
        type: "POST",
        url: "clients/delete/"+$(this).val(),
        data: $(this).val(),
        async: false,
        cache: false,
        success: function(data) {
          location.reload();
        }
      });
      return false;
    });
  });

  function open_add_form(item = null) {

    if (item === null) {

      $("#id").val("");
      $("#nombre").val("");
      $("#apellido").val("");
      $("#email").val("");
      $("#observaciones").val("");
      $("#idGrupoCliente option:selected").attr("selected", false);

    } else {

      $("#id").val(item['id']);
      $("#nombre").val(item['nombre']);
      $("#apellido").val(item['apellido']);
      $("#email").val(item['email']);
      $("#observaciones").val(item['observaciones']);
      $("#idGrupoCliente option[value=" + item['id_grupo_cliente'] + "]").attr("selected", true);
    }

    $("#modal_client_form").modal()
  }
</script>
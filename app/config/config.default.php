<?php
/**
 * 
 * En este archivo deben configurarse definen los datos de base de datos
 * y URL del sitio en produción o en desarrollo.
 * Una vez configurado cambiar el nombre por config.php
 * 
 */

//DB Params
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'posts');
//App Root
define('APPROOT', dirname(dirname(__FILE__)));
define('URLROOT', 'http://localhost/frameworkpost/');
//site name
define('SITENAME', 'Posts');
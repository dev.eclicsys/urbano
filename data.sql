-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         5.7.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando datos para la tabla urbano.clientes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`nombre`, `apellido`, `email`, `id_grupo_cliente`, `observaciones`) VALUES
	('Camilo', 'Sapienza', 'camilosapienza@gmail.com', 1, NULL),
	('Martina', 'Sapienza', 'box2@mail.tld', 2, NULL),
	('Patricio', 'Sapienza', 'box3@tld.com', 3, NULL);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando datos para la tabla urbano.clientes_grupos: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes_grupos` DISABLE KEYS */;
INSERT INTO `clientes_grupos` (`nombre`, `descripcion`) VALUES
	('One', NULL),
	('Two', 'Hello World'),
	('Three', 'Hi!');
/*!40000 ALTER TABLE `clientes_grupos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
